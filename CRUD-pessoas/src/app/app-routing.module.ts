import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PessoasComponent } from './pessoas/pessoas.component'
import { DashboardComponent } from './dashboard/dashboard.component';
import { DetalhesPessoaComponent } from './detalhes-pessoa/detalhes-pessoa.component';
import { NovoCadastroComponent } from './novo-cadastro/novo-cadastro.component';
import { ResultadosComponent } from './resultados/resultados.component'

const rotas: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'pessoas', component: PessoasComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'detalhe/:id', component:DetalhesPessoaComponent},
  {path: 'novo', component:NovoCadastroComponent},
  {path: 'resultado', component:ResultadosComponent}
]

@NgModule({
  imports: [ RouterModule.forRoot(rotas)],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
