import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api'
import { InMemoryDataService } from './in-memory-data.service'

import { AppComponent } from './app.component';
import { PessoasComponent } from './pessoas/pessoas.component';

import { DetalhesPessoaComponent } from './detalhes-pessoa/detalhes-pessoa.component';
import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { NovoCadastroComponent } from './novo-cadastro/novo-cadastro.component';
import { PessoaBuscaComponent } from './pessoa-busca/pessoa-busca.component';
import { ResultadosComponent } from './resultados/resultados.component';
import { NgxChartsModule, PieChartModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';





@NgModule({
  declarations: [
    AppComponent,
    PessoasComponent,
    DetalhesPessoaComponent,
    MessagesComponent,
    DashboardComponent,
    NovoCadastroComponent,
    PessoaBuscaComponent,
    ResultadosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxChartsModule,
    PieChartModule,
    BrowserAnimationsModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    //HttpClientInMemoryWebApiModule.forRoot(
    //  InMemoryDataService, { dataEncapsulation: false }
    //)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
