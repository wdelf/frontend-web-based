import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import{ Pessoa } from '../pessoas/pessoas'
import{ PessoaService } from '../pessoa.service'

@Component({
  selector: 'app-pessoa-busca',
  templateUrl: './pessoa-busca.component.html',
  styleUrls: ['./pessoa-busca.component.css']
})
export class PessoaBuscaComponent implements OnInit {

  pessoas$: Observable<Pessoa[]>;
  private searchTerms = new Subject<string>();
 
  constructor(private pessoaService: PessoaService) {}
 
  // Push a search term into the observable stream.
  buscaPessoa(term: string): void {
    this.searchTerms.next(term);
  }
 
  ngOnInit(): void {
    this.pessoas$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
 
      // ignore new term if same as previous term
      distinctUntilChanged(),
 
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.pessoaService.buscaPessoas(term)),
    );
  }

}
