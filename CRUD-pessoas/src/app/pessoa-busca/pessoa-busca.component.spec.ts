import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PessoaBuscaComponent } from './pessoa-busca.component';

describe('PessoaBuscaComponent', () => {
  let component: PessoaBuscaComponent;
  let fixture: ComponentFixture<PessoaBuscaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PessoaBuscaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PessoaBuscaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
