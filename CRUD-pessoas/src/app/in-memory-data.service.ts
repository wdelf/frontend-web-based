import { InMemoryDbService } from 'angular-in-memory-web-api'

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
      const pessoas = [
        {id: 11, nome: 'Maria0', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 12, nome: 'Maria1', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 13, nome: 'Maria2', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 14, nome: 'Maria3', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 15, nome: 'Maria4', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 16, nome: 'Maria5', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 17, nome: 'Maria6', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 18, nome: 'Maria7', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 19, nome: 'Maria8', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'},
        {id: 20, nome: 'Maria9', data_nasc: new Date(1936, 5, 15), documento: 123456789, sexo: 2, endereco: 'Rua 1'}
      ];
      return {pessoas};
    }
  }