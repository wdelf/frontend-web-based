import { Component, OnInit } from '@angular/core';

import { Pessoa } from '../pessoas/pessoas'
import { PessoaService} from '../pessoa.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  pessoas: Pessoa [] = [];

  constructor(private pessoaService: PessoaService) { }

  ngOnInit() {
    this.getPessoas();
  }

  getPessoas(): void {
    this.pessoaService.getPessoas().subscribe(pessoas => this.pessoas = pessoas.slice(pessoas.length - 4, pessoas.length))
  }

}
