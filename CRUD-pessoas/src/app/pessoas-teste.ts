import { Pessoa } from './pessoas/pessoas'

export const PESSOAS: Pessoa[] = [
    {id: 11, nome: 'Maria0', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 12, nome: 'Maria1', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 13, nome: 'Maria2', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 14, nome: 'Maria3', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 15, nome: 'Maria4', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 16, nome: 'Maria5', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 17, nome: 'Maria6', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 18, nome: 'Maria7', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 19, nome: 'Maria8', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'},
    {id: 20, nome: 'Maria9', dataNascimento: new Date(1936, 5, 15), identificacao: 123456789, sexo: 2, endereco: 'Rua 1'}
];