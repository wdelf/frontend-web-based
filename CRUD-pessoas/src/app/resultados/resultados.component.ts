import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { Pessoa } from '../pessoas/pessoas'
import { PessoaService } from '../pessoa.service'
import { MessagesService} from '../messages.service'

export const sequential = [
  '#448AFF',
  '#2979FF',
  '#90CAF9',
  '#64B5F6',
  '#42A5F5',
  '#2196F3',
  '#1E88E5',
  '#1976D2',
  '#1565C0',
  '#0D47A1',
  '#82B1FF',
  '#2962FF'
];

export var single = [];

export var data = [];

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.css']
})
export class ResultadosComponent implements OnInit {

  pessoas: Pessoa[];

  single: any[];

  view: any[] = [500, 300];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Idade';
  showYAxisLabel = true;
  yAxisLabel = 'Pessoas';

  showXAxis2 = true;
  showYAxis2 = true;
  gradient2 = false;
  showLegend2 = true;
  showXAxisLabel2 = true;
  xAxisLabel2 = 'Sexo';
  showYAxisLabel2 = true;
  yAxisLabel2 = 'Pessoas';

  data: any[];
  colorScheme = { domain: sequential };

  //colorScheme = {
  //  domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  //};

  constructor(
    private pessoaService: PessoaService,
    private messageService: MessagesService
  ) {}

  onSelect(event) {
    console.log(event);
  }

  ngOnInit() {
    this.getPessoas();
  }

  getPessoas(): void {
    this.pessoaService.getPessoas().subscribe(pessoas => this.pessoas = pessoas,
      err => {
      },
      () => {
           this.atualizaGraf();
      });
  }

  pushIdade(nome:string, valor: number): void{
    single.push({"name": nome, "value": valor})
  }

  contaPessoas(idadeMin: number, idadeMax: number): number{
    switch(idadeMin){
      case 0: { 
        return this.pessoas.filter(this.calcIdade09).length;
      } 
      case 10: { 
        return this.pessoas.filter(this.calcIdade19).length;
      } 
      case 20: { 
        return this.pessoas.filter(this.calcIdade29).length;
      } 
      case 30: { 
        return this.pessoas.filter(this.calcIdade39).length; 
      } 
      case 40: { 
        return this.pessoas.filter(this.calcIdade40).length;
      } 
    }
  }

  calcIdade09(pessoa: Pessoa, idadeMin: number, idadeMax): any{
    let now = new Date().getFullYear();
    let data = new Date(pessoa.dataNascimento);
    return(now - data.getFullYear() >= 0 && now - data.getFullYear() <= 9);
    //return(now - data.getFullYear() >= idadeMin && now - data.getFullYear() <= idadeMax);
  }

  calcIdade19(pessoa: Pessoa, idadeMin: number, idadeMax): any{
    let now = new Date().getFullYear();
    let data = new Date(pessoa.dataNascimento);
    return(now - data.getFullYear() >= 10 && now - data.getFullYear() <= 19);
    //return(now - data.getFullYear() >= idadeMin && now - data.getFullYear() <= idadeMax);
  }

  calcIdade29(pessoa: Pessoa, idadeMin: number, idadeMax): any{
    let now = new Date().getFullYear();
    let data = new Date(pessoa.dataNascimento);
    return(now - data.getFullYear() >= 20 && now - data.getFullYear() <= 29);
    //return(now - data.getFullYear() >= idadeMin && now - data.getFullYear() <= idadeMax);
  }

  calcIdade39(pessoa: Pessoa, idadeMin: number, idadeMax): any{
    let now = new Date().getFullYear();
    let data = new Date(pessoa.dataNascimento);
    return(now - data.getFullYear() >= 30 && now - data.getFullYear() <= 39);
    //return(now - data.getFullYear() >= idadeMin && now - data.getFullYear() <= idadeMax);
  }

  calcIdade40(pessoa: Pessoa, idadeMin: number, idadeMax): any{
    let now = new Date().getFullYear();
    let data = new Date(pessoa.dataNascimento);
    return(now - data.getFullYear() >= 40 && now - data.getFullYear() <= 150);
    //return(now - data.getFullYear() >= idadeMin && now - data.getFullYear() <= idadeMax);
  }

  pushSexo(nome:string, valor: number): void{
    data.push({"name": nome, "value": valor})
  }

  contaSexo(tipo: number): number{
    switch(tipo){
      case 1: { 
        return this.pessoas.filter(this.calcSexM).length;
      } 
      case 2: { 
        return this.pessoas.filter(this.calcSexF).length;
      }
    }
  }

  calcSexM(pessoa: Pessoa): any{
    return(pessoa.sexo == 1);
  }

  calcSexF(pessoa: Pessoa): any{
    return(pessoa.sexo == 2);
  }

  atualizaGraf(): void{
    this.pushIdade("00-09", this.contaPessoas(0, 9));
    this.pushIdade("10-19", this.contaPessoas(10, 19));
    this.pushIdade("20-29", this.contaPessoas(20, 29));
    this.pushIdade("30-39", this.contaPessoas(30, 39));
    this.pushIdade("40+", this.contaPessoas(40, 150));
    this.pushSexo("Homens", this.contaSexo(1));
    this.pushSexo("Mulheres", this.contaSexo(2));
    Object.assign(this, { single });
    Object.assign(this, { data });
    this.messageService.clear();
  }
}
