export class Pessoa {
    id: number;
    nome: string;
    dataNascimento: Date;
    identificacao: number;
    sexo: number;
    endereco: string;
}