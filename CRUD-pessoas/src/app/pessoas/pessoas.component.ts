import { Component, OnInit } from '@angular/core';
import { Pessoa } from './pessoas'
import { PessoaService } from '../pessoa.service'

@Component({
  selector: 'app-pessoas',
  templateUrl: './pessoas.component.html',
  styleUrls: ['./pessoas.component.css']
})
export class PessoasComponent implements OnInit {

  pessoas: Pessoa[];

  constructor(private pessoaService: PessoaService) { }

  ngOnInit() {
    this.getPessoas();
  }

  getPessoas(): void {
    this.pessoaService.getPessoas().subscribe(pessoas => this.pessoas = pessoas);
  }

  deletePessoa(pessoa: Pessoa): void{
    this.pessoas = this.pessoas.filter(h => h !== pessoa);
    this.pessoaService.deletePessoa(pessoa).subscribe();
  }

  contaSexoMasc(name: 'testando'): void{
    
    console.log(this.pessoas.filter(this.masc).length);
  }

  masc(pessoa: Pessoa): any{
    return(pessoa.nome.includes("Luke"));
  }
}
