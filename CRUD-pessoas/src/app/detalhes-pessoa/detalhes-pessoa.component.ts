import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common"

import { Pessoa } from '../pessoas/pessoas'
import { PessoaService } from '../pessoa.service';


@Component({
  selector: 'app-detalhes-pessoa',
  templateUrl: './detalhes-pessoa.component.html',
  styleUrls: ['./detalhes-pessoa.component.css']
})
export class DetalhesPessoaComponent implements OnInit {

  @Input() pessoa: Pessoa;

  convertSexo(number): string{
    if (number == 1) return 'Masculino';
    else return 'Feminino';
  }

  constructor(
    private rota: ActivatedRoute,
    private pessoaService: PessoaService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getPessoa();
  }

  getPessoa(): void{
    const id = +this.rota.snapshot.paramMap.get('id')
    this.pessoaService.getPessoa(id).subscribe(pessoa => this.pessoa = pessoa);
  }

  voltar(): void{
    this.location.back();
  }

  salvar(): void{
    //this.pessoaService.atualizaPessoa(this.pessoa).subscribe(() => this.voltar())
    this.pessoaService.adicionaPessoa(this.pessoa).subscribe();
  }

  deletePessoa(pessoa: Pessoa): void{
    this.pessoaService.deletePessoa(pessoa).subscribe();
  }
}
