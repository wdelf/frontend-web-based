import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators'

import { Pessoa } from './pessoas/pessoas'
import { PESSOAS } from './pessoas-teste'
import { MessagesService } from './messages.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  pessoas: Observable<Pessoa[]>;
  private pessoasUrl = 'https://test-frontend-neppo.herokuapp.com/pessoas' //URL da API
  
  constructor(
    private http: HttpClient,
    private messageService: MessagesService
  ) { }

  //------------HTTP Requests

  //Carrega a lista de pessoas do servidor
  getPessoas(): Observable<Pessoa[]> {
    //this.messageService.add('Dados carregados com sucesso!');
    return this.http.get<Pessoa[]>(this.pessoasUrl + '.json').pipe(
      tap(pessoas => this.log('Dados Carregados')),
      catchError(this.handleError('getPessoas', []))
    );
  }

  //Carrega uma pessoa pelo seu ID
  getPessoa(id: number): Observable<Pessoa> {
    const url = `${this.pessoasUrl}/${id}`;
    //return of(PESSOAS.find(pessoa => pessoa.id === id));
    return this.http.get<Pessoa>(url + '.json').pipe(
      tap(_ => this.log(`Pessoa carregada id=${id}`)),
      catchError(this.handleError<Pessoa>(`getPessoa id=${id}`))
    );
  }

  //Atualiza Pessoa
  atualizaPessoa(pessoa: Pessoa): Observable<any> {
    return this.http.post(this.pessoasUrl, pessoa, httpOptions).pipe(
      tap(_ => this.log(`Dados atualizados id=${pessoa.id}`)),
      catchError(this.handleError<any>('atualizaPessoa'))
    );
  }

  //Adiciona uma Nova Pessoa
  adicionaPessoa(pessoa: Pessoa): Observable<Pessoa> {
    return this.http.post<Pessoa>(this.pessoasUrl, pessoa, httpOptions).pipe(
      tap((pessoa: Pessoa) => this.log(`Cadastro Adicionada id=${pessoa.id}`)),
      catchError(this.handleError<Pessoa>('adicionaPessoa id=${pessoa.id}'))
    )
  };

  addTeste():  Observable<any> {
    this.messageService.add("Testando aqui");
    return this.http.post(this.pessoasUrl, {
      nome:"Daiano",
      dataNascimento: new Date(1936, 5, 15).toDateString,
      identificacao:"666666666",
      sexo:"1",
      endereco:"PedroPedro"
    }, httpOptions).pipe(
      tap(_ => this.log(`Dados atualizados id`)),
      catchError(this.handleError<any>('atualizaPessoa')));
  }

  //Deleta Pessoa
  deletePessoa(pessoa: Pessoa | number): Observable<Pessoa> {
    const id = typeof pessoa === 'number' ? pessoa : pessoa.id;
    const url = `${this.pessoasUrl}/${id}`;

    return this.http.delete<Pessoa>(url, httpOptions).pipe(
      tap(_ => this.log(`Pessoa deltada id=${id}`)),
      catchError(this.handleError<Pessoa>('deletePessoa'))
    );
  } 

  //---------------Busca 
  buscaPessoas(term: string): Observable<Pessoa[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Pessoa[]>(`${this.pessoasUrl + '.json'}/?name=${term}`).pipe(
      tap(_ => this.log(`Encontrou os termos da busca "${term}"`)),
      catchError(this.handleError<Pessoa[]>('buscaPessoa', []))
    );
  }

  //---------------LOG's
  private log(mensagem: string){
    this.messageService.add(`PessoaService: ${mensagem}`)
  }

  //Erros
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
