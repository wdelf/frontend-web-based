import { Component, OnInit } from '@angular/core';

import { PessoaService} from '../pessoa.service'
import { Pessoa } from '../pessoas/pessoas'
import { MessagesService } from '../messages.service';

@Component({
  selector: 'app-novo-cadastro',
  templateUrl: './novo-cadastro.component.html',
  styleUrls: ['./novo-cadastro.component.css']
})
export class NovoCadastroComponent implements OnInit {

  pessoa: Pessoa;
  pessoaTeste: Pessoa ={
    id: 500,
    nome: "Pedro",
    dataNascimento: new Date(),
    identificacao: 987654321,
    sexo: 1,
    endereco: "rua dos bobos, 0"
  }

  constructor(
    private pessoaService: PessoaService,
    private messageService: MessagesService,
  ) { }

  ngOnInit() {
  }

  adicionar(
    nome: string,
    dataNascimento: Date,
    identificacao: number,
    sexo: number,
    endereco: string
  ): void{
    nome = nome.trim();
    if(!nome ){
      this.messageService.add("Nome em Branco!");
      return;
    }
    if(!identificacao ){
      this.messageService.add("Identificação em Branco!");
      return;
    }
    if(!endereco ){
      this.messageService.add("Endereço em Branco!");
      return;
    }
    if(!sexo ){
      this.messageService.add("Sexo nao Selecionado!");
      return;
    }
    if(!dataNascimento ){
      this.messageService.add("Data nao Selecionada!");
      return;
    }
    this.pessoaService.adicionaPessoa({nome, dataNascimento, identificacao, sexo, endereco} as Pessoa).subscribe();
  }

  add(): void{
    this.pessoaService.addTeste().subscribe();
  }

}
